//
//  ArchivedGoalView.swift
//  Todometer
//
//  Created by Mike Moran on 5/8/22.
//

import SwiftUI

struct ArchivedGoalView: View {
    @Binding var archivedGoal: Goal
    
    var body: some View {
        VStack(alignment: .leading) {
            Text(archivedGoal.name).font(.subheadline)
            
            HStack {
                Text("\(Image(systemName: "calendar"))")
                Text(archivedGoal.interval)
                
                Spacer()
                
                Label("\(Int(archivedGoal.totalDays))", systemImage: "hourglass.bottomhalf.fill")
                
            }
            .font(.caption)
        }
    }
}

struct ArchivedGoalView_Previews: PreviewProvider {
    static var previews: some View {
        ArchivedGoalView(archivedGoal: .constant(Goal.sampleGoal))
            .previewLayout(.sizeThatFits)
            .padding(32)
    }
}
