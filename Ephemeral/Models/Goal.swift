//
//  Goal.swift
//  todometer
//
//  Created by Mike Moran on 5/7/22.
//

import Foundation

struct Goal: Identifiable, Codable {
    let id: UUID
    var startDate: Date
    var endDate: Date
    var name: String
    var affirm: Double
    var reject: Double

    var totalDays: Int {
        let diffComponents = Calendar.current.dateComponents([.day], from: startDate, to: endDate)
        return diffComponents.day ?? 0
    }
    var interval: String {
        let formatter = DateIntervalFormatter()
        formatter.dateStyle = .medium
        formatter.timeStyle = .none

        return formatter.string(from: startDate, to: endDate)
    }

    init(
        id: UUID = UUID(),
        name: String,
        startDate: Date = Date.now,
        endDate: Date = Date.now,
        affirm: Double = 0,
        reject: Double = 0
    ) {
        self.id = id
        self.startDate = startDate
        self.endDate = Date.now
        self.name = name
        self.affirm = affirm
        self.reject = reject
    }
}

extension Goal {
    struct Data {
        var name: String = ""
        var startDate: Date = Date.now
        var endDate: Date = Date.now
        var affirm: Double = 0
        var reject: Double = 0
    }

    var data: Data {
        Data(name: name, affirm: affirm, reject: reject)
    }

    mutating func update(from data: Data) {
        name = data.name
        affirm = data.affirm
        reject = data.reject
        endDate = data.endDate
    }
}

extension Goal {
    static let sampleGoal: Goal = Goal(
        name: "Build an iOS app",
        startDate: Calendar.current.date(from: DateComponents(year: 2022, month: 1, day: 25))!,
        endDate: Calendar.current.date(from: DateComponents(year: 2022, month: 2, day: 14))!,
        affirm: 9,
        reject: 5
    )
}
