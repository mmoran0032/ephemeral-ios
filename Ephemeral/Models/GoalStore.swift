//
//  GoalStore.swift
//  todometer
//
//  Created by Mike Moran on 5/8/22.
//
// This is for files and such
// What happens if we want to save more things?
// I think we might want to save everything in a list, and use the first item
// as the "active" goal

import Foundation
import SwiftUI

class GoalStore: ObservableObject {
    @Published var goals: [Goal] = [Goal(name: "")]

    private static func fileURL() throws -> URL {
        try FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            .appendingPathComponent("goal.data")
    }

    static func load(completion: @escaping (Result<[Goal], Error>)->Void) {
        DispatchQueue.global(qos: .background).async {
            do {
                let fileURL = try fileURL()
                guard let file = try? FileHandle(forReadingFrom: fileURL) else {
                    DispatchQueue.main.async {
                        completion(.success([]))
                    }
                    return
                }
                let goals = try JSONDecoder().decode([Goal].self, from: file.availableData)
                DispatchQueue.main.async {
                    completion(.success(goals))
                }
            } catch {
                DispatchQueue.main.async {
                    completion(.failure(error))
                }
            }
        }
    }

    static func save(goals: [Goal], completion: @escaping (Result<Int, Error>)->Void) {
        DispatchQueue.global(qos: .background).async {
            do {
                let data = try JSONEncoder().encode(goals)
                let outfile = try fileURL()
                try data.write(to: outfile)
                DispatchQueue.main.async {
                    completion(.success(goals.count))
                }
            } catch {
                DispatchQueue.main.async {
                    completion(.failure(error))
                }
            }
        }
    }
}
