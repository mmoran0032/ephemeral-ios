//
//  GoalProgress.swift
//  todometer
//
//  Created by Mike Moran on 5/8/22.
//

import Foundation

class GoalProgress: ObservableObject {
    @Published var affirm: Double = 0
    @Published var reject: Double = 0
    @Published var startDate: Date?
    @Published var totalDays: Int?
    
    var target: Double { affirm + reject + .leastNonzeroMagnitude }
    var pctAffirm: Double { affirm / target }
    var pctReject: Double { reject / target }
    
    init(goal: Goal) {
        self.affirm = goal.affirm
        self.reject = goal.reject
        self.startDate = goal.startDate
        self.totalDays = goal.totalDays
    }
}

extension Goal {
    var progress: GoalProgress {
        GoalProgress(goal: self)
    }
}
