//
//  GoalEditView.swift
//  Ephemeral
//
//  Created by Mike Moran on 5/18/22.
//

import SwiftUI

struct GoalEditView: View {
    @Binding var goalName: String
    @Binding var goalStart: Date
    @Binding var isEditing: Bool
    
    var body: some View {
        VStack {
            Form {
                Section(header: Text("What is your goal?")) {
                    TextField("I want to accomplish...", text: $goalName)
                }
                
                Section(header: Text("When did you start?")) {
                    DatePicker(
                        "Start date",
                        selection: $goalStart,
                        displayedComponents: [.date]
                    )
                        .datePickerStyle(.graphical)
                }
            }
            
            Button(action: {
                    isEditing = false
                }) {
                    Text("Save")
                        .accessibilityLabel("Update current goal")
                        .font(.headline)
                        .padding()
                }
                .disabled(goalName.isEmpty)
        }
    }
}

struct GoalEditView_Previews: PreviewProvider {
    static var previews: some View {
        GoalEditView(goalName: .constant(""), goalStart: .constant(Date.now), isEditing: .constant(true))
            .previewLayout(.sizeThatFits)
            .padding(32)
        
        GoalEditView(goalName: .constant(Goal.sampleGoal.name), goalStart: .constant(Goal.sampleGoal.startDate), isEditing: .constant(true))
            .previewLayout(.sizeThatFits)
            .padding(32)
    }
}
